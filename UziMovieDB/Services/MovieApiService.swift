//
//  MovieApiService.swift
//  UziMovieDB
//
//  Created by Muchamad Fauzi on 07/12/22.
//

import Alamofire

final class MovieApiService: ApiService {
    var sessionManager: Session = Session.default
    
    func request(_ urlRequest: URLRequestConvertible) -> DataRequest {
        return sessionManager.request(urlRequest).validate(statusCode: 200..<600)
    }
    
}
