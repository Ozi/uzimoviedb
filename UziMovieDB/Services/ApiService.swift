//
//  ApiService.swift
//  UziMovieDB
//
//  Created by Muchamad Fauzi on 07/12/22.
//

import Alamofire

protocol ApiService {
    var sessionManager: Session { get set }
    
    func request(_ urlRequest: URLRequestConvertible) -> DataRequest
}
