//
//  MovieApiRouter.swift
//  UziMovieDB
//
//  Created by Muchamad Fauzi on 07/12/22.
//

import Foundation
import Alamofire

enum MovieApiRouter {
    case getGenres
    case getDiscover(genre: Int, page: Int)
    case getMovieDetails(movieId: Int)
}

extension MovieApiRouter: ApiRouter {
    
    var baseUrlString: String {
        return "https://api.themoviedb.org/3"
    }
    
    var apiKey: String {
        return "be8b6c8aa9a5f4e240bb6093f9849051"
    }
    
    var path: String {
        switch self {
            
        case .getGenres:
            return "/genre/movie/list"
        case .getDiscover:
            return "/discover/movie"
        case.getMovieDetails(let movieId):
            return "/movie/\(movieId)"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getGenres:
            return .get
        case .getDiscover:
            return .get
        case .getMovieDetails:
            return .get
        }
    }
    
    var headers: HTTPHeaders {
        return [
            "Content-Type": "application/json; charset=UTF-8"
        ]
    }
    
    var parameters: Parameters? {
        switch self {
        case .getGenres:
            return [
                "api_key": apiKey
            ]
        case .getDiscover(genre: let genre, page: let page):
            return [
                "api_key": apiKey,
                "with_genres": genre,
                "page": page
            ]
        case .getMovieDetails:
            return [
                "api_key": apiKey
            ]
        }
    }
    
    func body() throws -> Data? {
        switch self {
        case .getDiscover, .getGenres, .getMovieDetails:
            return nil
        }
    }
}
