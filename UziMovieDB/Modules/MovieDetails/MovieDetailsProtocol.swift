//
//  MovieDetailsProtocol.swift
//  UziMovieDB
//
//  Created by Muchamad Fauzi on 09/12/22.
//

protocol MovieDetailsViewToPresenterProtocol: AnyObject {
    var view: MovieDetailsPresenterToViewProtocol? { get set }
    var interactor: MovieDetailsPresenterToInteractorProtocol? { get set }
    var router: MovieDetailsPresenterToRouterProtocol? { get set }
    
    func fetchMovieDetails(movieId: Int)
}

protocol MovieDetailsPresenterToInteractorProtocol: AnyObject {
    var presenter: MovieDetailsInteractorToPresenterProtocol? { get set }
    func fetchMovieDetails(movieId: Int)
}

protocol MovieDetailsPresenterToViewProtocol: AnyObject {
    func didFetchMovieDetails(movieDetails: MovieDetails)
}

protocol MovieDetailsInteractorToPresenterProtocol: AnyObject {
    func didFetchMovieDetails(movieDetails: MovieDetails)
}

protocol MovieDetailsPresenterToRouterProtocol: AnyObject {
    func createModule() -> MovieDetailsVC
}
