//
//  MovieDetailsInteractor.swift
//  UziMovieDB
//
//  Created by Muchamad Fauzi on 09/12/22.
//

import Alamofire

class MovieDetailsInteractor: MovieDetailsPresenterToInteractorProtocol {
var presenter: MovieDetailsInteractorToPresenterProtocol?
    
    private lazy var httpService = MovieApiService()
    
}

extension MovieDetailsInteractor {
    
    func fetchMovieDetails(movieId: Int) {
        do {
            try MovieApiRouter
                .getMovieDetails(movieId: movieId)
                .request(usingHttpService: httpService)
                .responseJSON { (result) in
                    guard [200, 201].contains(result.response?.statusCode), let data = result.data else { return }

                    do {
                        let result = try JSONDecoder().decode(MovieDetails.self, from: data)

                        self.presenter?.didFetchMovieDetails(movieDetails: result)
                    } catch {
                        print("Something went wrong when parsing genres response with error = \(error)")
                    }
                }
        } catch {
            print("Something went wrong while fetching genre with error = \(error)")
        }
    }
}
