//
//  MovieDetailsVC.swift
//  UziMovieDB
//
//  Created by Muchamad Fauzi on 09/12/22.
//

import Kingfisher
import SnapKit
import UIKit

class MovieDetailsVC: UIViewController {
    var presentor: MovieDetailsViewToPresenterProtocol?
    
    private let scrollView: UIScrollView = {
    let view = UIScrollView()
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
    }()
    
    private let scrollStackViewContainer: UIStackView = {
    let view = UIStackView()
    view.axis = .vertical
    view.spacing = 10
    view.translatesAutoresizingMaskIntoConstraints = false
    return view
    }()
    
    
    
    var movieId: Int = 0
    var movieDetail: MovieDetails?
    var starImage = UIImageView()
    
    var poster: UIImageView = {
        let p = UIImageView()
        p.backgroundColor = UIColor.clear
        p.contentMode = .scaleToFill
        p.layer.cornerRadius = 10.0
        p.clipsToBounds = true
        
        return p
    }()
    
    let movieTitle = UILabel()
    let movieOverview = UILabel()
    let movieRating = UILabel()
    let movieGenres = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.systemBackground
        
        setupScrollView()
//        setupViews()
        fetchMovieDetails()
    }
    
    func fetchMovieDetails() {
        presentor?.fetchMovieDetails(movieId: self.movieId)
    }
    
    private func setupScrollView() {
        let margins = view.layoutMarginsGuide
        view.addSubview(scrollView)
        scrollView.addSubview(scrollStackViewContainer)
        
        scrollView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(5)
            make.trailing.equalToSuperview().offset(-10)
        }
        scrollView.topAnchor.constraint(equalTo: margins.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: margins.bottomAnchor).isActive = true
        scrollStackViewContainer.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor).isActive = true
        scrollStackViewContainer.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor).isActive = true
        scrollStackViewContainer.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        scrollStackViewContainer.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        scrollStackViewContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        
        setupViews()
    }
    
    func setupViews() {
        scrollStackViewContainer.addArrangedSubview(poster)
        poster.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(30)
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(10)
            make.height.equalTo(500)
        }
        
        movieTitle.font = .boldSystemFont(ofSize: 24)
        scrollStackViewContainer.addArrangedSubview(movieTitle)
        movieTitle.snp.makeConstraints { make in
            make.top.equalTo(poster.snp.bottom).offset(10)
            make.left.equalTo(10)
            make.right.equalTo(-10)
        }
        
        movieRating.font = .boldSystemFont(ofSize: 20)
        movieRating.textColor = UIColor.white
        scrollStackViewContainer.addSubview(movieRating)
        movieRating.snp.makeConstraints { make in
            make.top.equalTo(poster.snp.bottom).inset(32)
            make.right.equalTo(-20)
        }
        
        starImage.image = UIImage(named: "ic_star")
        scrollStackViewContainer.addSubview(starImage)
        starImage.snp.makeConstraints { make in
            make.top.equalTo(poster.snp.bottom).inset(32)
            make.right.equalTo(movieRating.snp.left).offset(-5)
            make.size.equalTo(22)
        }
        
        movieGenres.font = .systemFont(ofSize: 20)
        movieGenres.textColor = UIColor.gray
        scrollStackViewContainer.addArrangedSubview(movieGenres)
        movieGenres.snp.makeConstraints { make in
            make.top.equalTo(movieTitle.snp.bottom).offset(3)
            make.left.equalTo(10)
            make.width.equalTo(290)
            
        }
        
        movieOverview.font = .systemFont(ofSize: 14)
        movieOverview.numberOfLines = 20
        scrollStackViewContainer.addArrangedSubview(movieOverview)
        movieOverview.snp.makeConstraints { make in
            make.top.equalTo(movieGenres.snp.bottom).offset(8)
            make.left.equalTo(10)
            make.right.equalTo(-10)
            
        }
    
    }
  
}

extension MovieDetailsVC: MovieDetailsPresenterToViewProtocol {

    func didFetchMovieDetails(movieDetails: MovieDetails) {
        
        let url = URL(string: "http://image.tmdb.org/t/p/w500\(movieDetails.movieImageUrl)")
        
        DispatchQueue.main.async {
            self.movieDetail = movieDetails
            self.movieTitle.text = movieDetails.movieTitle
            self.movieOverview.text = movieDetails.movieOverview
            self.poster.kf.setImage(with: url)
            self.movieRating.text = String(movieDetails.voteAverage).maxLength(length: 3)
            self.movieGenres.text = "\(movieDetails.genres.map{$0.name }.map{String($0)}.joined(separator: ", "))"
        }
    }

}

extension String {
   func maxLength(length: Int) -> String {
       var str = self
       let nsString = str as NSString
       if nsString.length >= length {
           str = nsString.substring(with:
               NSRange(
                location: 0,
                length: nsString.length > length ? length : nsString.length)
           )
       }
       return  str
   }
}
