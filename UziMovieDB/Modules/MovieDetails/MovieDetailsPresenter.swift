//
//  MovieDetailsPresenter.swift
//  UziMovieDB
//
//  Created by Muchamad Fauzi on 09/12/22.
//

class MovieDetailsPresenter: MovieDetailsViewToPresenterProtocol {
    
    
    var view: MovieDetailsPresenterToViewProtocol?
    var router: MovieDetailsPresenterToRouterProtocol?
    var interactor: MovieDetailsPresenterToInteractorProtocol?
    
    func fetchMovieDetails(movieId: Int) {
        interactor?.fetchMovieDetails(movieId: movieId)
    }
    
}

extension MovieDetailsPresenter: MovieDetailsInteractorToPresenterProtocol {
    
    func didFetchMovieDetails(movieDetails: MovieDetails) {
        view?.didFetchMovieDetails(movieDetails: movieDetails)
    }
    
}
