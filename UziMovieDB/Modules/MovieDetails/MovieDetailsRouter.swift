//
//  MovieDetailsRouter.swift
//  UziMovieDB
//
//  Created by Muchamad Fauzi on 09/12/22.
//

import UIKit

public class MovieDetailsRouter: MovieDetailsPresenterToRouterProtocol{
    
    
    public static let shared = MovieDetailsRouter()
    
    func initialize() -> MovieDetailsVC {
        return createModule()
    }
    
    func createModule() -> MovieDetailsVC {
        let view = MovieDetailsVC()
        
        let presenter: MovieDetailsViewToPresenterProtocol & MovieDetailsInteractorToPresenterProtocol = MovieDetailsPresenter()
        
        let interactor: MovieDetailsPresenterToInteractorProtocol = MovieDetailsInteractor()
        
        let router: MovieDetailsPresenterToRouterProtocol = MovieDetailsRouter()
        
        view.presentor = presenter
        presenter.view = view 
        presenter.router = router
        presenter.interactor = interactor
        
        interactor.presenter = presenter
        
        return view
    }

}
