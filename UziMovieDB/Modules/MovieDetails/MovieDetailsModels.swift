//
//  MovieDetailsModels.swift
//  UziMovieDB
//
//  Created by Muchamad Fauzi on 09/12/22.
//

struct genre: Decodable {
    var id: Int
    var name: String
}

struct MovieDetails: Decodable {
    
    var movieId: Int
    var movieOverview: String
    var movieImageUrl: String
    var movieTitle: String
    var voteAverage: Double
    var genres: [genre]

    
    enum CodingKeys: String, CodingKey {
        case movieTitle    = "title"
        case movieOverview = "overview"
        case movieImageUrl = "poster_path"
        case movieId = "id"
        case voteAverage = "vote_average"
        case genres = "genres"
    }
}
