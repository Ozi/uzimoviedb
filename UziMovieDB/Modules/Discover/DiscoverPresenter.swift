//
//  DiscoverPresenter.swift
//  UziMovieDB
//
//  Created by Muchamad Fauzi on 07/12/22.
//

class DiscoverPresenter: DiscoverViewToPresenterProtocol {
    
    
    var view: DiscoverPresenterToViewProtocol?
    var router: DiscoverPresenterToRouterProtocol?
    var interactor: DiscoverPresenterToInteractorProtocol?

    func goToMovieDetails(movieId: Int, from: DiscoverVC) {
        
    }
    
    func goToMovieDetails(movieId: Int, movies: [Movie], from: DiscoverVC) {
        router?.goToMovieDetails(movieId: movieId, movies: movies, from: from)
    }
}

extension DiscoverPresenter: DiscoverInteractorToPresenterProtocol {
    func didFetchDiscover(movies: [Movie], page: Int, totalPages: Int) {
        view?.didFetchDiscover(movies: movies, page: page, totalPages: totalPages)
    }
    
    func fetchDiscover(genreId: Int, page: Int) {
        interactor?.fetchDiscover(genreId: genreId, page: page)
    }
}
