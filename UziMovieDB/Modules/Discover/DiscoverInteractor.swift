//
//  DiscoverInteractor.swift
//  UziMovieDB
//
//  Created by Muchamad Fauzi on 07/12/22.
//

import Alamofire

class DiscoverInteractor: DiscoverPresenterToInteractorProtocol {
    var presenter: DiscoverInteractorToPresenterProtocol?
    
    private lazy var httpService = MovieApiService()
}

extension DiscoverInteractor {
    func fetchDiscover(genreId: Int, page: Int) {
        do {
            try MovieApiRouter
                .getDiscover(genre: genreId, page: page)
                .request(usingHttpService: httpService)
                .responseJSON { (result) in
                    let (movies, page, totalPages) = self.parseDiscover(result: result)
                    
                    self.presenter?.didFetchDiscover(movies: movies, page: page, totalPages: totalPages)
                }
        } catch {
            print("Something went wrong while fetching genre with error = \(error)")
        }
    }
}


extension DiscoverInteractor {
    private func parseDiscover(result: DataResponse<Any, AFError>) -> ([Movie], Int, Int) {
        guard [200, 201].contains(result.response?.statusCode), let data = result.data else { return ([], 0, 0) }
        do {
            let result = try JSONDecoder().decode(DiscoverResponse.self, from: data)
            return (result.movies, result.page, result.totalPages)
        } catch {
            print("Something went wrong when parsing genres response with error = \(error)")
        }

        return ([], 0, 0)
    }
}
