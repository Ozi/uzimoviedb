//
//  DiscoverRouter.swift
//  UziMovieDB
//
//  Created by Muchamad Fauzi on 07/12/22.
//

import UIKit

public class DiscoverRouter: DiscoverPresenterToRouterProtocol{
    
    public static let shared = DiscoverRouter()
    
    func initialize() -> DiscoverVC {
        return createModule()
    }
    
    func createModule() -> DiscoverVC {
        let view = DiscoverVC()
        
        let presenter: DiscoverViewToPresenterProtocol & DiscoverInteractorToPresenterProtocol = DiscoverPresenter()
        
        let interactor: DiscoverPresenterToInteractorProtocol = DiscoverInteractor()
        
        let router: DiscoverPresenterToRouterProtocol = DiscoverRouter()
        
        view.presentor = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        
        interactor.presenter = presenter
        
        return view
    }
    
    func goToMovieDetails(movieId: Int, movies: [Movie], from: DiscoverVC) {
        let vc = MovieDetailsRouter().createModule()
        vc.movieId = movieId
        from.navigationController?.pushViewController(vc, animated: true)
    }
}
