//
//  DiscoverProtocol.swift
//  UziMovieDB
//
//  Created by Muchamad Fauzi on 07/12/22.
//

protocol DiscoverViewToPresenterProtocol: AnyObject {
    var view: DiscoverPresenterToViewProtocol? { get set }
    var interactor: DiscoverPresenterToInteractorProtocol? { get set }
    var router: DiscoverPresenterToRouterProtocol? { get set }
    
    func fetchDiscover(genreId: Int, page: Int)
    func goToMovieDetails(movieId: Int, movies: [Movie], from: DiscoverVC)
}

protocol DiscoverPresenterToRouterProtocol: AnyObject {
    func createModule() -> DiscoverVC
    func goToMovieDetails(movieId: Int, movies: [Movie], from: DiscoverVC)
}

protocol DiscoverPresenterToViewProtocol: AnyObject {
    func didFetchDiscover(movies: [Movie], page: Int, totalPages: Int)
}

protocol DiscoverInteractorToPresenterProtocol: AnyObject {
    func didFetchDiscover(movies: [Movie], page: Int, totalPages: Int)
}

protocol DiscoverPresenterToInteractorProtocol: AnyObject {
    var presenter: DiscoverInteractorToPresenterProtocol? { get set }
    func fetchDiscover(genreId: Int, page: Int) -> Void
}
