//
//  GenresEntity.swift
//  UziMovieDB
//
//  Created by Muchamad Fauzi on 07/12/22.
//

import Foundation

struct Genres: Decodable {
    let genres: [Genre]
    
    private enum CodingKeys: String, CodingKey {
        case genres = "genres"
    }
}

struct Genre: Decodable {
    let id: Int
    let name: String
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
    }
}
