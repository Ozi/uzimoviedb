//
//  GenresRouter.swift
//  UziMovieDB
//
//  Created by Muchamad Fauzi on 07/12/22.
//

import UIKit

public class GenresRouter: GenresPresenterToRouterProtocol{
    public static let shared = GenresRouter()
    
    func initialize() -> GenresVC {
        return createModule()
    }
    
    func createModule() -> GenresVC {
        let view = GenresVC()
        
        let presenter: GenresViewToPresenterProtocol & GenresInteractorToPresenterProtocol = GenresPresenter()
        
        let interactor: GenresPresenterToInteractorProtocol = GenresInteractor()
        
        let router: GenresPresenterToRouterProtocol = GenresRouter()
        
        view.presentor = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        
        interactor.presenter = presenter
        
        return view
    }
    
    func goToDiscoveries(genreId: Int, title: String, from: GenresVC) {
        let vc = DiscoverRouter().createModule()
        vc.genreId = genreId
        vc.title = title
        from.navigationController?.pushViewController(vc, animated: true)
    }
}
