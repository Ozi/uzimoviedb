//
//  GenresProtocol.swift
//  UziMovieDB
//
//  Created by Muchamad Fauzi on 07/12/22.
//

protocol GenresViewToPresenterProtocol: AnyObject {
    var view: GenresPresenterToViewProtocol? { get set }
    var interactor: GenresPresenterToInteractorProtocol? { get set }
    var router: GenresPresenterToRouterProtocol? { get set }
    
    func fetchGenres() -> Void
    func goToDiscoveries(genreId: Int, title: String, from: GenresVC)
}

protocol GenresPresenterToRouterProtocol: AnyObject {
    func createModule() -> GenresVC
    func goToDiscoveries(genreId: Int, title: String, from: GenresVC)
}

protocol GenresPresenterToViewProtocol: AnyObject {
    func didFetchGenres(genres: [Genre])
}

protocol GenresInteractorToPresenterProtocol: AnyObject {
    func didFetchGenres(genres: [Genre])
}

protocol GenresPresenterToInteractorProtocol: AnyObject {
    var presenter: GenresInteractorToPresenterProtocol? { get set }
    func fetchGenres() -> Void
}
