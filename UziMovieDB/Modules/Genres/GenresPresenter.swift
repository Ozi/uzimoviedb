//
//  GenresPresenter.swift
//  UziMovieDB
//
//  Created by Muchamad Fauzi on 07/12/22.
//

class GenresPresenter: GenresViewToPresenterProtocol {
    
    var view: GenresPresenterToViewProtocol?
    var router: GenresPresenterToRouterProtocol?
    var interactor: GenresPresenterToInteractorProtocol?
    
    func goToDiscoveries(genreId: Int, title: String, from: GenresVC) {
        router?.goToDiscoveries(genreId: genreId, title: title, from: from)
    }
}

extension GenresPresenter: GenresInteractorToPresenterProtocol {
    func didFetchGenres(genres: [Genre]) {
        view?.didFetchGenres(genres: genres)
    }
    
    func fetchGenres() {
        interactor?.fetchGenres()
    }
}
