//
//  GenresInteractor.swift
//  UziMovieDB
//
//  Created by Muchamad Fauzi on 07/12/22.
//

import Alamofire

class GenresInteractor: GenresPresenterToInteractorProtocol {

    
    var httpService = MovieApiService()
    
    var presenter: GenresInteractorToPresenterProtocol?
    
    func fetchGenres()-> Void {
        do {
            try MovieApiRouter
                .getGenres
                .request(usingHttpService: httpService)
                .responseJSON { (result) in
                    guard [200, 201].contains(result.response?.statusCode), let data = result.data else { return }
                    do {
                        let result = try JSONDecoder().decode(Genres.self, from: data)
                        self.presenter?.didFetchGenres(genres: result.genres)
                    } catch {
                        print("Something went wrong when parsing genres response with error = \(error)")
                    }
                }
        } catch {
            print("Something went wrong while fetching genre with error = \(error)")
        }
    }
}
