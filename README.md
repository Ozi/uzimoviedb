# UziMovieDB
iOS App using The Movie DB API

# Description
Hello there, this app build by me Muchamad Fauzi as iOS Developer
- This project used VIPER architecture and Programmatically

# Feature
- Shows a list of Genres Movies
- Shows a list of Movies
- Show Movie Details ( In this view, ScrollView was added. So if you used iPhone small screen, you still can enjoy it)

## Command
don't forget to run this command
```pod install```
